/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.utilities;

import org.model.Message;

/**
 * @author aguswinarno
 */
public class Constants {

    public static class Size {
        public final static double MINIMUM_WINDOW_WIDTH = 500;
        public final static double MINIMUM_WINDOW_HEIGHT = 500;
    }

    public static class Layout {

        public final static Integer LOGIN = 0;
        public final static Integer MAIN = 1;

        public final static Integer MAIN_PROFILE = 3;
        public final static Integer INPUT_PROFILE = 31;


        public final static String PROFILE_SCR = "org/layout/userManagement.fxml";
        public final static String INPUT_PROFILE_SCR = "org/layout/userProfile.fxml";

        public final static String LOGIN_SCR = "org/layout/login.fxml";
        public final static String MAIN_SCR = "org/layout/main.fxml";
    }

    public static class Menu {

    }

    public static class RestResponse {
        public final static Message DEFAULT = new Message();
    }

    public static class RestUtil {
        public final static String BASE_URL = "http://localhost/uifx";

        public final static String USERS_URL = "/user/";
    }

    public static class Parameter {
        public final static String ACTION = "ACTION";
        public final static String ACTION_EDIT = "EDIT";
        public final static String ACTION_NEW = "NEW";

        public final static String OBJECT = "OBJECT";
    }

}
