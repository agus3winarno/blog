/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model;

import org.apache.http.HttpStatus;

/**
 * @author aguswinarno
 */
public class Message {

    private String code;
    private String message;

    public Message(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public Message() {
        this.code = String.valueOf(HttpStatus.SC_OK);
        this.message = "OK";
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
