/*
 * Copyright (c) 2012, Oracle and/or its affiliates. All rights reserved.
 */
package org;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Dialogs;
import javafx.scene.layout.Pane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import org.controller.BaseController;
import org.exception.UiFXException;
import org.security.Authenticator;
import org.utilities.Constants;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.utilities.Constants.Layout.*;

/**
 * Main Application. This class handles navigation and user session.
 */
public class Main extends Application {

    private Stage stage;
    private final double MINIMUM_WINDOW_WIDTH = Constants.Size.MINIMUM_WINDOW_WIDTH;
    private final double MINIMUM_WINDOW_HEIGHT = Constants.Size.MINIMUM_WINDOW_HEIGHT;

    private Map<String, Object> pageParameter;

    private static final Map<Integer, String> map = new HashMap<>();

    {
        map.put(MAIN, MAIN_SCR);
        map.put(LOGIN, LOGIN_SCR);
        map.put(MAIN_PROFILE, PROFILE_SCR);
        map.put(INPUT_PROFILE, INPUT_PROFILE_SCR);

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Application.launch(Main.class, (String[]) null);
    }

    public Main() {
        this.pageParameter = new HashMap<>();
    }

    @Override
    public void start(Stage primaryStage) {
        try {
            stage = primaryStage;
            stage.setTitle("UiFX - Demo");
            stage.setMinWidth(MINIMUM_WINDOW_WIDTH);
            stage.setMinHeight(MINIMUM_WINDOW_HEIGHT);

            translateTo(LOGIN, null, null);
            primaryStage.show();
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean userLogging(String userId, String password) {
        try {
            if (Authenticator.validate(userId, password)) {
                translateTo(MAIN, null, null);
                return true;
            }
        } catch (UiFXException gbe) {
            Dialogs.showErrorDialog(getStage(), gbe.getMessage());
        }
        return false;
    }

    public void userLogout() {
        translateTo(LOGIN, null, null);
    }

    private void translateToScreen(String xml, Double height, Double width) {
        try {
            BaseController controller = (BaseController) replaceSceneContent(xml, height, width);
            controller.setApp(this);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void translateToFullScreen(String xml) {
        try {
            BaseController controller = (BaseController) replaceSceneContentFullScreen(xml);
            controller.setApp(this);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private Initializable replaceSceneContent(String fxml, double height, double width) {
        FXMLLoader loader = new FXMLLoader();
        InputStream in = getClass().getClassLoader().getResourceAsStream(fxml);
        loader.setBuilderFactory(new JavaFXBuilderFactory());
        loader.setLocation(getClass().getClassLoader().getResource(fxml));
        Pane pane = null;
        try {
            pane = (Pane) loader.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                //e.printStackTrace();
            }
        }

        Rectangle2D rectangle2D = Screen.getPrimary().getVisualBounds();

        Scene scene = new Scene(pane, height, width);
        stage.setScene(scene);
        stage.sizeToScene();
        if (!String.valueOf(stage.getWidth()).equalsIgnoreCase("NaN") && !String.valueOf(stage.getHeight()).equalsIgnoreCase("NaN")) {
            stage.setX((rectangle2D.getWidth() - stage.getWidth()) / 2);
            stage.setY((rectangle2D.getHeight() - stage.getHeight()) / 4);
        }
        return (Initializable) loader.getController();
    }

    private Initializable replaceSceneContentFullScreen(String fxml) {
        FXMLLoader loader = new FXMLLoader();
        InputStream in = getClass().getClassLoader().getResourceAsStream(fxml);
        loader.setBuilderFactory(new JavaFXBuilderFactory());
        loader.setLocation(getClass().getClassLoader().getResource(fxml));
        Pane pane = null;
        try {
            pane = (Pane) loader.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                //e.printStackTrace();
            }
        }

        Rectangle2D rectangle2D = Screen.getPrimary().getVisualBounds();

        Scene scene = new Scene(pane, rectangle2D.getWidth(), rectangle2D.getHeight()-20);

        stage.setX(0);
        stage.setY(20);
        stage.setScene(scene);
        stage.sizeToScene();
        return (Initializable) loader.getController();
    }

    public void translateToWithDefaultScreen(Integer screen) {

        Double height = MINIMUM_WINDOW_HEIGHT;

        Double width = MINIMUM_WINDOW_WIDTH;

        translateToScreen(map.get(screen), height, width);
    }

    public void translateTo(Integer screen, Double height, Double width) {

        if (null == height) height = MINIMUM_WINDOW_HEIGHT;

        if (null == width) width = MINIMUM_WINDOW_WIDTH;

        translateToScreen(map.get(screen), height, width);
    }

    public void translateToFullScreen(Integer screen) {

        translateToFullScreen(map.get(screen));
    }

    public Stage getStage() {
        return stage;
    }

    public void clearPageParameter() {
        if (pageParameter != null) pageParameter.clear();
    }

    public void addPageParameter(String key, Object value) {
        pageParameter.put(key, value);
    }

    public Map<String, Object> getPageParameter() {
        return this.pageParameter;
    }

}
