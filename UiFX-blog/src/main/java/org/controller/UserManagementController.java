/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Dialogs;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import org.Main;
import org.controller.rest.UserService;
import org.model.User;
import org.utilities.Constants;
import org.utilities.Constants.Layout;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * FXML Controller class
 *
 * @author aguswinarno
 */
public class UserManagementController extends BaseController {

    @FXML
    private TableView<User> userTable;
    @FXML
    private TableColumn<User, String> userName;
    @FXML
    private TableColumn<User, String> email;
    @FXML
    private TableColumn<User, String> keterangan;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        super.initialize(url, rb);

        userName.setCellValueFactory(new PropertyValueFactory<User, String>("username"));
        email.setCellValueFactory(new PropertyValueFactory<User, String>("email"));
        keterangan.setCellValueFactory(new PropertyValueFactory<User, String>("keterangan"));

        // Auto resize columns
        userTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

    }

    @Override
    protected void init() {
        super.init();
        userTable.setItems(UserService.getInstance().findAllEntity());
    }

    @Override
    public void setApp(Main application) {
        super.setApp(application);
    }

    public void create(ActionEvent actionEvent) {
        getApplication().addPageParameter(Constants.Parameter.ACTION, Constants.Parameter.ACTION_NEW);
        getApplication().translateToWithDefaultScreen(Layout.INPUT_PROFILE);
    }

    public void edit(ActionEvent actionEvent) {

        if(userTable.getSelectionModel().getSelectedItem() == null){
            Dialogs.showInformationDialog(getApplication().getStage(), "Belum ada data yang dipilih");
            return;
        }

        getApplication().addPageParameter(Constants.Parameter.ACTION, Constants.Parameter.ACTION_EDIT);
        getApplication().addPageParameter(Constants.Parameter.OBJECT, userTable.getSelectionModel().getSelectedItem());
        getApplication().translateToWithDefaultScreen(Layout.INPUT_PROFILE);
    }

    public void delete(ActionEvent actionEvent) {

        if(userTable.getSelectionModel().getSelectedItem() == null){
            Dialogs.showInformationDialog(getApplication().getStage(), "Belum ada data yang dipilih");
            return;
        }

        Integer id = userTable.getSelectionModel().getSelectedItem().getId();
        UserService.getInstance().remove(id);
        init();
        Dialogs.showInformationDialog(getApplication().getStage(), "Hapus data berhasil");
    }

    public void close(ActionEvent actionEvent) {
        getApplication().translateToWithDefaultScreen(Layout.MAIN);
    }

}
