/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.controller;

import javafx.event.ActionEvent;

import static org.utilities.Constants.Layout.MAIN_PROFILE;

/**
 * FXML Controller class
 *
 * @author aguswinarno
 */
public class MainController extends BaseController {

    public void user(ActionEvent event) {
        getApplication().translateToWithDefaultScreen(MAIN_PROFILE);
    }

    public void logout(ActionEvent event) {
        getApplication().userLogout();
    }

}
