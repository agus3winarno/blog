/*
 * Copyright (c) 2008, 2012 Oracle and/or its affiliates.
 * All rights reserved. Use is subject to license terms.
 *
 * This file is available and licensed under the following license:
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *  - Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the distribution.
 *  - Neither the name of Oracle Corporation nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Dialogs;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import org.Main;
import org.controller.rest.UserService;
import org.exception.PasswordException;
import org.exception.ValidateException;
import org.model.User;
import org.utilities.Constants;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Profile Controller.
 */
public class UserProfileController extends BaseFormControlller {

    @FXML
    private TextField username;

    @FXML
    private PasswordField password;

    @FXML
    private PasswordField confirmPassword;

    @FXML
    private TextField email;

    @FXML
    private TextField keterangan;

    private User data;

    @Override
    public void setApp(Main application) {
        super.setApp(application);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
    }

    @Override
    public void setTextField() {
        username.setText(data.getUsername());
        password.setText("");
        confirmPassword.setText("");
        email.setText(data.getEmail());
        keterangan.setText(data.getKeterangan());

        if (getActionStatus().equals(Constants.Parameter.ACTION_NEW)) {
            username.setEditable(true);
        }
    }

    @Override
    public void prepareData() {
        data = new User();

        if (getActionStatus().equals(Constants.Parameter.ACTION_EDIT))
            data = (User) getApplication().getPageParameter().get(Constants.Parameter.OBJECT);
    }

    @Override
    public void constructData() {
        data.setUsername(username.getText());
        data.setEmail(email.getText());
        data.setKeterangan(keterangan.getText());
        data.setPassword(password.getText());
    }

    @Override
    protected void init() {
        super.init();
    }

    public void save(ActionEvent event) {

        try {
            validateData();
            constructData();

            if (getActionStatus().equals(Constants.Parameter.ACTION_NEW)) {
                UserService.getInstance().createEntity(data);
                Dialogs.showInformationDialog(getApplication().getStage(), "Simpan data berhasil");
            } else {
                UserService.getInstance().updateEntity(data);
                Dialogs.showInformationDialog(getApplication().getStage(), "Ubah data berhasil");
            }

            close(event);
        } catch (PasswordException | ValidateException e) {
            Dialogs.showWarningDialog(getApplication().getStage(), e.getMessage());
        }


    }

    private void validateData() throws ValidateException, PasswordException {

        String usernameInput = username.getText();
        String emailInput = email.getText();
        String passwordInput = password.getText();
        String confirmPasswordInput = confirmPassword.getText();

        if (null == emailInput || emailInput.length() == 0)
            throw new ValidateException("Email belum diisi");

        if (null == usernameInput || usernameInput.length() == 0)
            throw new ValidateException("Username belum diisi");

        if (null == passwordInput || passwordInput.length() == 0)
            throw new ValidateException("Password belum diisi");

        if (null == confirmPasswordInput || confirmPasswordInput.length() == 0)
            throw new ValidateException("Confirm Password belum diisi");

        if (null != passwordInput && null != confirmPasswordInput && !passwordInput.equals(confirmPasswordInput))
            throw new PasswordException("Password & Confirm Password tidak sama");

    }

    public void close(ActionEvent event) {
        getApplication().clearPageParameter();
        getApplication().translateToWithDefaultScreen(Constants.Layout.MAIN_PROFILE);
    }

}
