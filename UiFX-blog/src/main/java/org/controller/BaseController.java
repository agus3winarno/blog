/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.controller;

import javafx.fxml.Initializable;
import javafx.scene.control.Dialogs;
import javafx.scene.layout.AnchorPane;
import org.Main;
import org.utilities.Constants;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;

/**
 * @author aguswinarno
 */
public abstract class BaseController extends AnchorPane implements Initializable {

    private SimpleDateFormat commonDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    private Main application;
    private Object actionStatus;

    public void setApp(Main application) {
        this.application = application;
        init();
    }

    public SimpleDateFormat getCommonDateFormat() {
        return commonDateFormat;
    }

    protected void init() {
        actionStatus = application.getPageParameter().get(Constants.Parameter.ACTION);
    }

    public Main getApplication() {
        return application;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

    public Object getActionStatus() {
        return actionStatus;
    }

    public void notYetImplemented(){
        Dialogs.showInformationDialog(getApplication().getStage(), "Belum diimplementasi");
    }
}
