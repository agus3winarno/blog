package org.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import javax.management.RuntimeErrorException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Login Controller.
 */
public class LoginController extends BaseController {

    @FXML
    TextField userId;
    @FXML
    PasswordField password;
    @FXML
    Button login;
    @FXML
    Label errorMessage;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        errorMessage.setText("");
        userId.setPromptText("");
        password.setPromptText("");

    }

    public void processLogin(ActionEvent event) {

        boolean login = false;

        if (getApplication() == null) {
            // We are running in isolated FXML, possibly in Scene Builder.
            // NO-OP.
            errorMessage.setText("Hello " + userId.getText());
        } else {

            try {
                login = getApplication().userLogging(userId.getText(), password.getText());
            } catch (RuntimeErrorException ex) {
                //nothing to do
            }

            if (!login) {
                errorMessage.setText("Username/Password is incorrect");
            }
        }
    }
}
