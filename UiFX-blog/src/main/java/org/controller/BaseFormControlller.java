package org.controller;

/**
 * Created by aguswinarno on 8/10/14.
 */
public abstract class BaseFormControlller extends BaseController {

    public abstract void setTextField();

    public abstract void prepareData();

    public abstract void constructData();

    @Override
    protected void init() {
        super.init();
        prepareData();
        setTextField();
    }
}
