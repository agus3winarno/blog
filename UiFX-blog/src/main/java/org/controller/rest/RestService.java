/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.controller.rest;

import javafx.collections.ObservableList;
import org.model.Message;

/**
 * @param <S>
 * @author aguswinarno
 */
public interface RestService<S> {

    Message createEntity(S s);

    Message updateEntity(S s);

    Message deleteEntity(S s);

    ObservableList<S> findAllEntity();

}
