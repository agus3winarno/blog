package org.controller.rest;

import com.google.gson.Gson;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.exception.UiFXException;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.jboss.resteasy.util.GenericType;
import org.utilities.Constants;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by aguswinarno on 6/16/14.
 */
public abstract class BaseRestService<T> {

    private Gson gson = new Gson();

    public String jsonString(T t) {
        return gson.toJson(t);
    }

    public T jsonToObject(String strJson, Class<T> clazz) {
        return gson.fromJson(strJson, clazz);
    }

    public void add(T t) {
        doRequest(createRequest().body(MediaType.APPLICATION_JSON, t), HttpPut.METHOD_NAME, null);
    }

    public T get(int id) {
        return (T) doRequest(createRequest(id), HttpGet.METHOD_NAME,
                new GenericType<T>() {
                });
    }

    public void remove(int id) {
        doRequest(createRequest(id), HttpDelete.METHOD_NAME, null);
    }

    public void update(T t) {
        doRequest(createRequest().body(
                MediaType.APPLICATION_JSON, t), HttpPost.METHOD_NAME, null);
    }

    public Object request(ClientRequest clientRequest, String httpMethod, GenericType<?> returnType) {
        return doRequest(clientRequest, httpMethod, returnType);
    }

    @SuppressWarnings("unchecked")
    public List<T> getAll(GenericType<?> returnType) {
        return (List<T>) doRequest(createRequest(), HttpGet.METHOD_NAME, returnType);
    }

    private ClientRequest createRequest(int id) {
        return new ClientRequest(UriBuilder.fromPath(baseUrl()).path(String.valueOf(id)).build().toString());
    }

    private ClientRequest createRequest() {
        return new ClientRequest(baseUrl());
    }

    private Object doRequest(ClientRequest cr, String httpMethod,
                             GenericType<?> returnType) {
        ClientResponse<?> r = null;
        Object serverResponseBody = null;
        try {
            Logger.getLogger(BaseRestService.class.getName()).log(Level.INFO, "PERFORM A " + httpMethod + " ON " + cr.getUri());
            r = cr.httpMethod(httpMethod);

            // ??
            int status = r.getStatus();
            if (status >= 500) {
                handleError("Server is having a bad time with this request, try again later...");
            } else if (status == 404) {
                handleError("Framework with ID "
                        + cr.getPathParameters().get("id") + " not found.");

                // if it's not a success code
            } else if (r.getStatus() >= 300) {
                handleError("The server responded with an unexpected status: " + r.getStatus());
            }
        } catch (Exception e) {
            Logger.getLogger(BaseRestService.class.getName()).log(Level.SEVERE, "EXCEPTION : ", e);
            handleError("Unknown error: " + e.getMessage());
        }
        if (returnType != null)
            serverResponseBody = r.getEntity(returnType);
        cr.clear();
        return serverResponseBody;
    }

    protected void handleError(String message) {
        throw new UiFXException(message);
    }

    public String baseUrl() {
        return Constants.RestUtil.BASE_URL;
    }

}
