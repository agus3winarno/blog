/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.controller.rest;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.jboss.resteasy.util.GenericType;
import org.model.Message;
import org.model.User;
import org.utilities.Constants;

import java.util.List;

/**
 * @author aguswinarno
 */
public class UserService extends BaseRestService<User> implements RestService<User> {

    private static UserService instance;

    public static UserService getInstance() {
        if(instance == null) instance = new UserService();
        return instance;
    }

    static ObservableList<User> userVOs = FXCollections.observableArrayList();

    @Override
    public Message createEntity(User s) {
        super.add(s);
        return Constants.RestResponse.DEFAULT;
    }

    @Override
    public Message updateEntity(User s) {
        super.update(s);
        return Constants.RestResponse.DEFAULT;
    }

    @Override
    public Message deleteEntity(User s) {
        super.remove(s.getId());
        return Constants.RestResponse.DEFAULT;
    }

    @Override
    public ObservableList<User> findAllEntity() {

        userVOs.clear();

        List<User> userVOs = super.getAll(new GenericType<List<User>>() {
        });

        UserService.userVOs.addAll(userVOs);

        return UserService.userVOs;
    }

    @Override
    public String baseUrl() {
        return (new StringBuffer(super.baseUrl()).append(Constants.RestUtil.USERS_URL).toString());
    }
}
