package org.exception;

/**
 * Created by aguswinarno on 9/9/14.
 */
public class UiFXException extends RuntimeException {

    public UiFXException() {
    }

    public UiFXException(String message) {
        super(message);
    }

    public UiFXException(String message, Throwable cause) {
        super(message, cause);
    }

    public UiFXException(Throwable cause) {
        super(cause);
    }

    public UiFXException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
